<?php
define('SHEET_TITLE','Portal(UK)_Data_Storage_Dev');
define('FEED_TITLE','Sheet1');
define('SHEET_TITLE_2','Content_Portal');
define('FEED_TITLE_2','Sheet1');
define('CLIENT_ID','118350200247590362241');
define('CLIENT_EMAIL','tamit-624@tamit-1287.iam.gserviceaccount.com');
define('PATH_P12',realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "Tamit-9bb698682835.p12");
require 'autoload.php';
$srcDir = realpath(__DIR__ . '/src/');
restore_include_path ();
set_include_path($srcDir . PATH_SEPARATOR . get_include_path());

spl_autoload_register(function ($class) {
	if (strpos($class, '\\') !== false) {
		@include str_replace("\\", "/", $class) . '.php';
	}
});

function getGoogleTokenFromKeyFile($clientId, $clientEmail, $pathToP12File) {
	$client = new Google_Client();
	$client->setClientId($clientId);

	$cred = new Google_Auth_AssertionCredentials(
			$clientEmail, array('https://spreadsheets.google.com/feeds'), file_get_contents($pathToP12File)
	);

	$client->setAssertionCredentials($cred);
	
	

	if ($client->getAuth()->isAccessTokenExpired()) {
		$client->getAuth()->refreshTokenWithAssertion($cred);
	}

	$service_token = json_decode($client->getAccessToken());
	return $service_token->access_token;
}

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;



$action = 'default';
if( count( $_POST ) > 0 || true){
	
	
	$sheet_title = SHEET_TITLE_2;			
	


	$clientId 			= CLIENT_ID;
	$clientEmail 		= CLIENT_EMAIL;
	$pathToP12File 		= PATH_P12;
	$accessToken 		= getGoogleTokenFromKeyFile($clientId, $clientEmail, $pathToP12File);
	
	

	$serviceRequest = new DefaultServiceRequest($accessToken);
	ServiceRequestFactory::setInstance($serviceRequest);

	$spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
	
	$spreadsheetFeed 	= $spreadsheetService->getSpreadsheets();
	
	echo '<pre>';
	print_r($spreadsheetFeed);
	die();
	
	$spreadsheet = $spreadsheetFeed->getByTitle($sheet_title);
	$worksheetFeed = $spreadsheet->getWorksheets();
	
	$worksheet = $worksheetFeed->getByTitle(FEED_TITLE_2);
	$listFeed = $worksheet->getListFeed();
	
	
	

	
	
	
	$values = array();
	$i = 0;
	foreach ($listFeed->getEntries() as $entry) {
		$values[$i] = $entry->getValues();
		$i++;
	}
	$new_values = array();
	if( count( $values ) > 0 ){
		foreach( $values as $value ){
			$new_values[$value['sheetid']] = $value;
		}
		$values = $new_values;
	}
	
	

	
	$xhtml = "";
	if( $action == "edit_word" ){
		echo $_POST['value'];
		die();
	}
	
	if( $action == "download_word" ){
		require_once dirname(__FILE__).'/PHPWord/bootstrap.php';
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		
		$row_index 		= $_POST['row_index'];
		$slider_index 	= $_POST['slider_index'];

		
		$value = $values[$row_index];
		$return['status'] = 1;
		$xhtml = $value["heading"];
		if( isset( $_POST['heading_html'] ) && $_POST['heading_html'] != "" ){
			$xhtml = $_POST["heading_html"];
		}
		
		if( isset( $_POST['file_name'] ) && $_POST['file_name'] != "" ){
			$file_name 		= $_POST['file_name'];
		}else{
			$file_name 		= $value["firstname"].'_'.$value["lastname"].'_'.$_POST['lang'].'.docx';
		}
		$txt_file = $value["firstname"].'_'.$value["lastname"].'_'.$_POST['lang'].'.txt';
		$sliders = explode('<hr>',$value["profiletext"]);				
	
		

		if( isset( $_POST['content_html'] ) && $_POST['content_html'] != "" ){
			$xhtml .= $_POST["content_html"];
		}else{
			if( is_array( $sliders ) && count( $sliders ) > 0 ){
				$xhtml .= $sliders[$slider_index];
			}	
		}
		/*
		$xhtml .= 	$_POST["content_html"];
		*/
		$xhtml = str_replace('<br>','<w:br />',$xhtml);
		$xhtml = str_replace('<br/>','<w:br />',$xhtml);
		$xhtml = str_replace('<b>','<strong>',$xhtml);
		$xhtml = str_replace('<B>','<strong>',$xhtml);
		$xhtml = str_replace('</b>','</strong>',$xhtml);
		$xhtml = str_replace('</B>','</strong>',$xhtml);
		$xhtml = str_replace('<hr>','',$xhtml);
		$xhtml = str_replace('<HR>','',$xhtml);
		$xhtml = str_replace('<p></p>','',$xhtml);
		$xhtml = str_replace('<p>Click to edit</p>','',$xhtml);
		$xhtml = str_replace('<p><p>','<p>',$xhtml);
		$xhtml = str_replace('</p></p>','</p>',$xhtml);
		$xhtml = str_replace('&','and',$xhtml);
		$xhtml = str_replace('_','',$xhtml);
		$xhtml = strip_tags($xhtml,'<p><strong>');
		

	
		file_put_contents( dirname(__FILE__).'/files/'.$txt_file,$xhtml );
		
		
		$indexlistFeed = $worksheet->getListFeed(["sq" => "sheetid = $row_index", "reverse" => "true"]);
		$entries = $indexlistFeed->getEntries();
		$listEntry = $entries[0];
		$cols = $listEntry->getValues();
		$cols["finalcontent"] = $xhtml;
		$listEntry->update($cols);
		
		
		
		$section = $phpWord->addSection();
		
		// Add first page header
		$header = $section->addHeader();

		$table = $header->addTable();

		$table->addRow();

		$cell = $table->addCell(4500);

		$table->addCell(4500)->addImage('resources/logo.png', array('width' => 210, 'height' => 100, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::END));
		
		\PhpOffice\PhpWord\Shared\Html::addHtml($section, $xhtml);
		
		$saved = $phpWord->save(dirname(__FILE__).'/files/'.$file_name);
		
	
		
		$return['status'] 	 = 1;
		$return['file_name'] = $file_name;
		$return['txt_file'] = $txt_file;
		$return['url'] 		 = url().'/webhook/files/'.$file_name;
		echo json_encode($return);
		die();

		
		
	}
	if( $action == "preview_pdf" ){
		include dirname(__FILE__).'/tcpdf/tcpdf.php';
		
		
		$row_index 		= $_POST['row_index'];
		
		
		
		$slider_index 	= $_POST['slider_index'];
		if( isset( $_POST['file_name'] ) && $_POST['file_name'] != "" ){
			$file_name 		= $_POST['file_name'];
		}else{
			$file_name = time().'.pdf';
		}
		

		
		$value = $values[$row_index];
		$return['status'] = 1;
		$xhtml = $value["heading"];
		$sliders = explode('<hr>',$value["profiletext"]);
		$slider_html = '';
		if( is_array( $sliders ) && count( $sliders ) > 0 ){
			$xhtml .= '<div class="sheet-professional">
				'.$sliders[$slider_index].'
			</div>';
		}
		$xhtml .= $slider_html;
		

		
		//update row
		$indexlistFeed = $worksheet->getListFeed(["sq" => "sheetid = $row_index", "reverse" => "true"]);
		$entries = $indexlistFeed->getEntries();
		$listEntry = $entries[0];
		$cols = $listEntry->getValues();
		$cols["finalcontent"] = $xhtml;
		$listEntry->update($cols);
		

		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('ThelifeCode Team');
		$pdf->SetTitle($value["firstname"].'\'s profile');
		$pdf->SetSubject($value["firstname"].'\'s profile');
		$pdf->SetKeywords($value["firstname"].'\'s profile');
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $value["firstname"].'\'s profile', "by Webapi - Realtorprofiler.site \nwww.realtorprofiler.site");
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->AddPage();
		//$pdf->SetFont('helvetica', 'B', 20);
		//$pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);
		//$html = 'hello';
		//$pdf->SetFont('helvetica', '', 10);

		// output the HTML content
		//$pdf->writeHTML($xhtml);

		//$pdf->Ln();

		// set UTF-8 Unicode font
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->writeHTML($xhtml, true, 0, true, true);
		$pdf->lastPage();
		
		/*
			I: send the file inline to the browser. The PDF viewer is used if available.
			D: send to the browser and force a file download with the name given by name.
			F: save to a local file with the name given by name (may include a path).
			S: return the document as a string.
		*/
		$pdf->Output(dirname(__FILE__).'/files/'.$file_name, 'F');
		$return['status'] 	 = 1;
		$return['file_name'] = $file_name;
		$return['url'] 		 = url().'/webhook/files/'.$file_name;
		$return['xhtml'] 	 = '<embed src="'.$return["url"].'" type="application/pdf" width="100%" height="500px">';
		echo json_encode($return);
		die();
	}
	
	if( $action == "get_row_info" ){
		$row_index 		= $_POST['row_index'];
		
		$value = $values[$row_index];
		$xhtml = "";
		if( $value ){
			$return['status'] = 1;
			$xhtml = $value["heading"];

			$sliders = explode('<hr>',$value["profiletext"]);
			$slider_html = '';
			if( is_array( $sliders ) && count( $sliders ) > 0 ){
				$slider_html .= '<div class="sheet-professional"><div class="owl-carousel owl-theme show-owl-nav">';
				foreach( $sliders as $slider ){
					$slider_html .= '<div class="item">'.$slider.'</div>';
				}
				$slider_html .= '</div></div>';
			}
			$xhtml .= $slider_html;
		}else{
			$return['status'] = 0;
		}
		
		
		$return['xhtml'] = $xhtml;
		$return['value'] = $value;	
		echo json_encode($return);
		die();
	}
	if( $action == "get_list_feeds" ){
		$user_id = $_POST['user_id'];
		$data_users = array();
		if( count( $values ) > 0 ){
			foreach( $values as $key => $value ){
				if( $value["portalid"] == $user_id ){
					$link 	= url().'/single-googlesheet/?sheetid='.$key;
					$xhtml .= '<div class="w-row w-body">
							<div class="w-cell">'.$value["portalid"].'</div>
							<div class="w-cell">'.$value["firstname"].'</div>
							<div class="w-cell">'.$value["lastname"].'</div>
							
							<div class="w-cell"><a target="_blank" href="'.$link.'">View</a></div>
						</div>';
					$data_users[] = $key;
				}
			}
		}
		
		if( count( $data_users ) == 0 ){
			$xhtml = 'No record found';
		}
		
		$return['status'] = 1;
		$return['values'] = $values;	
		$return['xhtml']  = $xhtml;	
		
		echo json_encode($return);
		die();
	}
}	
	
	
/*



if( $data->form_response && false ){
	$answers = $data->form_response->answers;
	$row = array();
	if( count( $answers ) > 0 ){
		foreach( $answers as $answer ){
			$id = $answer->field->id;
			$type  = $answer->type;
			switch ($type) {
				case 'text':
				  $value = $answer->text; 
				  break;
				 case 'number':
				  $value = $answer->number; 
				  break;
				case 'email':
				  $value = $answer->email; 
				  break;
				case 'url':
				  $value = $answer->url; 
				  break;
				case 'choices':
				   $value = $answer->choices->labels; 
				   $value = implode(',',$value);
				  break;
				case 'choice':
				   $value = $answer->choice->label; 
				  break;
				default:
				  $value = $answer->text; 
				  break;
			}
			
			switch ($id) {
				case '55537621':
				  $row['userid'] = $value;
				  break;
				case 'CPDH':
				  $row['firstname'] = $value;
				  break;
				case 'qjDm':
				  $row['lastname'] = $value;
				  break;
				case 'uufy':
				  $row['officialjobtitle'] = $value;
				  break;
				case '54041088':
				  $row['whichofthefollowingbestdescribesyourrolepleaseselectone'] = $value;
				  break;
				default:
				  # code...
				  break;
			}
		}
		
	}
	
	if( count( $row ) > 0 ){
		$clientId 			= CLIENT_ID;
		$clientEmail 		= CLIENT_EMAIL;
		$pathToP12File 		= PATH_P12;
		$accessToken 		= getGoogleTokenFromKeyFile($clientId, $clientEmail, $pathToP12File);



		$serviceRequest = new DefaultServiceRequest($accessToken);
		ServiceRequestFactory::setInstance($serviceRequest);

		$spreadsheetService = new Google\Spreadsheet\SpreadsheetService();

		$spreadsheetFeed 	= $spreadsheetService->getSpreadsheets();
		
		$spreadsheet = $spreadsheetFeed->getByTitle('Realtorprofiler Api');
		$worksheetFeed = $spreadsheet->getWorksheets();
		
		$worksheet = $worksheetFeed->getByTitle('Sheet1');
		$listFeed = $worksheet->getListFeed();
		
		/*
		$values = "";
		foreach ($listFeed->getEntries() as $entry) {
			$values = $entry->getValues();
		}
		
		
	}	
}

*/

