<?php
/**
 * Single variation cart button
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="woocommerce-variation-add-to-cart variations_button">
	<?php
		/**
		 * @since 3.0.0.
		 */
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
		) );

		/**
		 * @since 3.0.0.
		 */
		do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>
	<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
<style>
.progress {
    height: 20px;
    margin-bottom: 20px;
    overflow: hidden;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}
.limited-edition-group .progress {
    height: 40px;
    border-radius: 4px;
    max-width: 100%;
	 margin: 0;
}
.progress-bar {
    float: left;
    width: 0;
    height: 100%;
    font-size: 16px;
    line-height: 35px;
    color: #fff;
    text-align: center;
    background-color: #428bca;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
    box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
    -webkit-transition: width .6s ease;
    transition: width .6s ease;
}
.progress-bar-danger {
    background-color: black;
    color: white;
}
.yith-wcwl-add-button.show {
    display: none !important;
}
</style>
<div class="limited-edition-group">
	<div class="limited_edition_wrapper">
	  <!--
	  <span class="limited_edition_info">Limited edition:</span>
	  <span class="limited_edition_info">ONLY</span>
	  -->
	  <span class="limited_edition_info limited_edition_quantity" style="display:none;" id="limited_edition_quantity">1000</span>
	  <span class="limited_edition_info limited_edition_quantity" id="sold">0</span>
	  <span class="limited_edition_info">sold within 24 hours</span>
	</div>
	<div class="progress">
	  <div id="limited_edition_bar" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1000" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"><span id="sold_current">0</span> of 1000 sold</div>
	</div>

	<script type="text/javascript">
	  var limited_edition_interval;
	  jQuery( document ).ready(function($){
		var post_id = '<?php echo get_the_ID();?>';
		
		var quantity = $.cookie('limited_edition_quantity_'+post_id); 
		hack_show(quantity);
		function hack_show( quantity ){
			if(quantity == null) {
				quantity = 700;
				$.cookie('limited_edition_quantity_'+post_id, quantity, { expires: 1 });
			}
			
			var percent = Math.floor(parseInt(quantity, 10) * 100 /  1000);
			$('#limited_edition_quantity').html(quantity);
			$('#limited_edition_bar').attr('aria-valuenow', percent).css('width', percent + '%');
			
			var current_quantity = $('#limited_edition_quantity').html();

			current_quantity = parseInt(current_quantity, 10) - Math.round(Math.random());
			$.cookie('limited_edition_quantity_'+post_id, current_quantity, { expires: 1 });
			var current_percent = Math.floor(parseInt(current_quantity, 10) * 100 /  1000);
			$('#limited_edition_quantity').html(current_quantity);
			$('#sold_current').html(current_quantity);
			$('#sold').html( 1000 - parseInt(current_quantity) );
			$('#limited_edition_bar').attr('aria-valuenow', current_percent).css('width', current_percent + '%');
			$('#limited_edition_bar').addClass('progress-bar-striped').addClass('active');      
			setTimeout(function(){$('#limited_edition_bar').removeClass('progress-bar-striped').removeClass('active');}, 500);
		}
		

		
	  });
	</script>
</div>

<div id="count-section">
	<style>
	.ux-timer > span:first-child {
		display: none !important;
	}
	</style>
	<?php
		$y = date('Y');
		$m = date('m') + 1;
		$d = date('d');
	?>
	<?php echo do_shortcode('[ux_countdown year="'.$y.'" month="'.$m.'" day="'.$d.'" time="19:00"]');?>
</div>
