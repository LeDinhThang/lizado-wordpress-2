<?php
/**
 * Function include all files in folder
 *
 * @param $path   Directory address
 * @param $ext    array file extension what will include
 * @param $prefix string Class prefix
 */
if ( ! function_exists( 'vi_include_folder' ) ) {
	function vi_include_folder( $path, $prefix = '', $ext = array( 'php' ) ) {

		/*Include all files in payment folder*/
		if ( ! is_array( $ext ) ) {
			$ext = explode( ',', $ext );
			$ext = array_map( 'trim', $ext );
		}
		$sfiles = scandir( $path );
		foreach ( $sfiles as $sfile ) {
			if ( $sfile != '.' && $sfile != '..' ) {
				if ( is_file( $path . "/" . $sfile ) ) {
					$ext_file  = pathinfo( $path . "/" . $sfile );
					$file_name = $ext_file['filename'];
					if ( $ext_file['extension'] ) {
						if ( in_array( $ext_file['extension'], $ext ) ) {
							$class = preg_replace( '/\W/i', '_', $prefix . ucfirst( $file_name ) );

							if ( ! class_exists( $class ) ) {
								require_once $path . $sfile;
								if ( class_exists( $class ) ) {
									new $class;
								}
							}
						}
					}
				}
			}
		}
	}
}
if ( ! function_exists( 'woo_notification_prefix' ) ) {
	function woo_notification_prefix() {
		$prefix = get_option( '_woo_notification_prefix', date( "Ymd" ) );

		return $prefix . '_products';
	}
}

if ( ! function_exists( 'woocommerce_notification_check_pro_version' ) ) {
	function woocommerce_notification_check_pro_version() {
		if ( is_plugin_active( 'woocommerce-notification/woocommerce-notification.php' ) ) {
			return true;
		}

		return false;
	}
}

if ( ! function_exists( 'woocommerce_notification_wpversion' ) ) {
	function woocommerce_notification_wpversion() {
		global $wp_version;
		if ( version_compare( $wp_version, '4.5.0', '<=' ) ) {
			return true;
		} else {
			false;
		}
	}
}
/**
 *
 * @param string $version
 *
 * @return bool
 */
if ( ! function_exists( 'woocommerce_version_check' ) ) {
	function woocommerce_version_check($version = '3.0'){
		global $woocommerce;

		if ( version_compare( $woocommerce->version, $version, ">=" ) ) {
			return true;
		}
		return false;
	}
}