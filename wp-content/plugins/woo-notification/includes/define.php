<?php
define( 'VI_WNOTIFICATION_F_DIR', WP_PLUGIN_DIR . DIRECTORY_SEPARATOR . "woo-notification" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_ADMIN', VI_WNOTIFICATION_F_DIR . "admin" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_FRONTEND', VI_WNOTIFICATION_F_DIR . "frontend" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_LANGUAGES', VI_WNOTIFICATION_F_DIR . "languages" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_INCLUDES', VI_WNOTIFICATION_F_DIR . "includes" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_TEMPLATES', VI_WNOTIFICATION_F_DIR . "templates" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_CACHE', VI_WNOTIFICATION_F_DIR . "caches" . DIRECTORY_SEPARATOR );

define( 'VI_WNOTIFICATION_F_SOUNDS', VI_WNOTIFICATION_F_DIR . "sounds" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_SOUNDS_URL', WP_PLUGIN_URL . "/woo-notification/sounds/" );

define( 'VI_WNOTIFICATION_F_CSS', WP_PLUGIN_URL . "/woo-notification/css/" );
define( 'VI_WNOTIFICATION_F_CSS_DIR', VI_WNOTIFICATION_F_DIR . "css" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_JS', WP_PLUGIN_URL . "/woo-notification/js/" );
define( 'VI_WNOTIFICATION_F_JS_DIR', VI_WNOTIFICATION_F_DIR . "js" . DIRECTORY_SEPARATOR );
define( 'VI_WNOTIFICATION_F_IMAGES', WP_PLUGIN_URL . "/woo-notification/images/" );


/*Include functions file*/
if ( is_file( VI_WNOTIFICATION_F_INCLUDES . "functions.php" ) ) {
	require_once VI_WNOTIFICATION_F_INCLUDES . "functions.php";
}
/*Include functions file*/
if ( is_file( VI_WNOTIFICATION_F_INCLUDES . "ads.php" ) ) {
	require_once VI_WNOTIFICATION_F_INCLUDES . "ads.php";
}

vi_include_folder( VI_WNOTIFICATION_F_ADMIN, 'VI_WNOTIFICATION_F_Admin_' );
vi_include_folder( VI_WNOTIFICATION_F_FRONTEND, 'VI_WNOTIFICATION_F_Frontend_' );
?>