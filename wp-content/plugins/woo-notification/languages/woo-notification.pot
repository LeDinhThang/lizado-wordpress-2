#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-06 08:50+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: Volumes/DATA/SVN/woo-notification/trunk/woo-notification.php:41
msgid ""
"Please install WooCommerce and active. WooCommerce Notification is going to "
"working."
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/admin.php:56
msgid "Upgrade Woo Notification Premium"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/admin.php:119
#: Volumes/DATA/SVN/woo-notification/trunk/admin/admin.php:119
msgid "Settings"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/admin.php:122
#: Volumes/DATA/SVN/woo-notification/trunk/admin/admin.php:122
msgid "Upgrade Premium"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/admin.php:160
msgid "WooCommerce Notification"
msgstr ""

#. Name of the plugin
#: Volumes/DATA/SVN/woo-notification/trunk/admin/admin.php:161
msgid "Woo Notification"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:191
msgid "General"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:192
msgid "Design"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:193
msgid "Products"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:195
msgid "Product detail"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:196
msgid "Time"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:197
msgid "Sound"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:198
msgid "Assign"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:199
msgid "Logs"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:203
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:209
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:215
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:221
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:227
msgid "Only activate on"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:204
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:210
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:216
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:222
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:228
#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:310
msgid "Pro version "
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:238
msgid "Enable"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:261
msgid "Select Product"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:282
msgid "Please select products"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:309
msgid "Add more products with "
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:316
msgid "Virtual First Name"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:321
msgid ""
"Virtual first name what will show on notification. Each first name on a line."
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:326
msgid "Virtual Time"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:332
#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:337
msgid "hours"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:335
msgid "Time will auto get random in this time threshold ago."
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:340
msgid "Virtual City"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:344
msgid ""
"Virtual city name what will show on notification. Each city name on a line."
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:349
msgid "Virtual Country"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:353
msgid "Virtual country name what will show on notification."
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:366
msgid "Message purchased"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:374
msgid "Customer first name"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:377
msgid "Customer city"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:381
msgid "Customer country"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:385
msgid "Product title"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:389
msgid "Product title with link"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:393
msgid "Time after purchase"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:400
msgid "Position"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:413
msgid "Bottom left"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:425
msgid "Bottom right"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:436
msgid "Top left"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/settings.php:447
msgid "Top right"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:17
#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:83
#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:84
msgid "System Status"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:59
#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:59
msgid "PHP Time Limit"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:63
#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:63
msgid "PHP Max Input Vars"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:68
#: Volumes/DATA/SVN/woo-notification/trunk/admin/system.php:68
msgid "Memory Limit"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:154
msgid "About"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:154
msgid "ago"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:328
msgid "days"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:330
msgid "day"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:339
msgid "hour"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:346
msgid "minutes"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:348
msgid "minute"
msgstr ""

#: Volumes/DATA/SVN/woo-notification/trunk/frontend/notify.php:351
msgid "seconds"
msgstr ""

#. Description of the plugin
msgid ""
"Increase conversion rate by highlighting other customers that have bought "
"products."
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "http://villatheme.com"
msgstr ""

#. Author of the plugin
msgid "Andy Ha (villatheme.com)"
msgstr ""
