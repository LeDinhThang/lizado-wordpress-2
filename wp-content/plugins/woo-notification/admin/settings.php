<?php

/*
Class Name: WP_SM_Admin_Settings
Author: Andy Ha (support@villatheme.com)
Author URI: http://villatheme.com
Copyright 2016 villatheme.com. All rights reserved.
*/

class VI_WNOTIFICATION_F_Admin_Settings {
	static $params;

	public function __construct() {
		add_action( 'admin_init', array( $this, 'save_meta_boxes' ) );
		add_action( 'wp_ajax_wcnf_search_product', array( $this, 'search_product' ) );
	}

	/*Ajax Search*/
	public function search_product( $x = '', $post_types = array( 'product' ) ) {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		ob_start();

		$keyword = filter_input( INPUT_GET, 'keyword', FILTER_SANITIZE_STRING );

		if ( empty( $keyword ) ) {
			die();
		}
		$arg            = array(
			'post_status'    => 'publish',
			'post_type'      => $post_types,
			'posts_per_page' => 50,
			's'              => $keyword

		);
		$the_query      = new WP_Query( $arg );
		$found_products = array();
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$prd = wc_get_product( get_the_ID() );

				if ( $prd->has_child() && $prd->is_type( 'variable' ) ) {
					$product_children = $prd->get_children();
					if ( count( $product_children ) ) {
						foreach ( $product_children as $product_child ) {
							if ( woocommerce_version_check() ) {
								$product = array(
									'id'   => $product_child,
									'text' => get_the_title( $product_child )
								);

							} else {
								$child_wc  = wc_get_product( $product_child );
								$get_atts  = $child_wc->get_variation_attributes();
								$attr_name = array_values( $get_atts )[0];
								$product   = array(
									'id'   => $product_child,
									'text' => get_the_title() . ' - ' . $attr_name
								);

							}
							$found_products[] = $product;
						}

					}
				} else {
					$product          = array( 'id' => get_the_ID(), 'text' => get_the_title() );
					$found_products[] = $product;
				}
			}
		}
		wp_send_json( $found_products );
		die;
	}


	/**
	 * Get files in directory
	 *
	 * @param $dir
	 *
	 * @return array|bool
	 */
	static private function scan_dir( $dir ) {
		$ignored = array( '.', '..', '.svn', '.htaccess', 'test-log.log' );

		$files = array();
		foreach ( scandir( $dir ) as $file ) {
			if ( in_array( $file, $ignored ) ) {
				continue;
			}
			$files[ $file ] = filemtime( $dir . '/' . $file );
		}
		arsort( $files );
		$files = array_keys( $files );

		return ( $files ) ? $files : false;
	}

	/**
	 * Save post meta
	 *
	 * @param $post
	 *
	 * @return bool
	 */
	public function save_meta_boxes() {
		if ( ! isset( $_POST['_wnotification_nonce'] ) || ! isset( $_POST['wnotification_params'] ) ) {
			return false;
		}
		if ( ! wp_verify_nonce( $_POST['_wnotification_nonce'], 'wnotification_save_email_settings' ) ) {
			return false;
		}
		if ( ! current_user_can( 'manage_options' ) ) {
			return false;
		}
		update_option( '_woo_notification_prefix', substr( md5( date( "YmdHis" ) ), 0, 10 ) );
		update_option( 'wnotification_params', $_POST['wnotification_params'] );
	}

	/**
	 * Set Nonce
	 * @return string
	 */
	protected static function set_nonce() {
		return wp_nonce_field( 'wnotification_save_email_settings', '_wnotification_nonce' );
	}

	/**
	 * Set field in meta box
	 *
	 * @param      $field
	 * @param bool $multi
	 *
	 * @return string
	 */
	protected static function set_field( $field, $multi = false ) {
		if ( $field ) {
			if ( $multi ) {
				return 'wnotification_params[' . $field . '][]';
			} else {
				return 'wnotification_params[' . $field . ']';
			}
		} else {
			return '';
		}
	}

	/**
	 * Get Post Meta
	 *
	 * @param $field
	 *
	 * @return bool
	 */
	public static function get_field( $field, $default = '' ) {
		if ( self::$params ) {
			$params = self::$params;
		} else {
			$params       = get_option( 'wnotification_params', array() );
			self::$params = $params;

		}

		if ( isset( $params[ $field ] ) && $field ) {
			return $params[ $field ];
		} else {
			return $default;
		}
	}

	/**
	 * Get list shortcode
	 * @return array
	 */
	public static function page_callback() { ?>
		<div class="wrap woo-notification">
			<h2><?php esc_attr_e( 'Woo Notification Settings', 'wp-send-end' ) ?></h2>
			<div class="top-help">
				<a target="_blank" class="button"
				   href="https://villatheme.com/supports/forum/plugins/woocommerce-notification/"><?php esc_html_e( 'Get Support', 'woo-notification' ) ?></a>
				<a target="_blank" class="button"
				   href="http://docs.villatheme.com/?item=woocommerce-notification"><?php esc_html_e( 'Documentation', 'woo-notification' ) ?></a>
			</div>
			<form method="post" action="" class="vi-ui form">
				<?php echo ent2ncr( self::set_nonce() ) ?>

				<div class="vi-ui attached tabular menu">
					<div class="item active"
						 data-tab="general"><?php esc_html_e( 'General', 'woo-notification' ) ?></div>
					<div class="item" data-tab="design"><?php esc_html_e( 'Design', 'woo-notification' ) ?></div>
					<div class="item" data-tab="products"><?php esc_html_e( 'Products', 'woo-notification' ) ?></div>
					<div class="item"
						 data-tab="product-detail"><?php esc_html_e( 'Product detail', 'woo-notification' ) ?></div>
					<div class="item" data-tab="time"><?php esc_html_e( 'Time', 'woo-notification' ) ?></div>
					<div class="item" data-tab="sound"><?php esc_html_e( 'Sound', 'woo-notification' ) ?></div>
					<div class="item" data-tab="assign"><?php esc_html_e( 'Assign', 'woo-notification' ) ?></div>
					<div class="item" data-tab="logs"><?php esc_html_e( 'Logs', 'woo-notification' ) ?></div>
				</div>
				<div class="vi-ui bottom attached tab segment" data-tab="time">
					<p>
						<?php esc_html_e( 'Only activate on', 'woo-notification' ) ?>
						<a href="https://goo.gl/PwXTzT"><?php echo esc_html__( 'Pro version ', 'woo-notification' ) ?></a>
					</p>
				</div>
				<div class="vi-ui bottom attached tab segment" data-tab="product-detail">
					<p>
						<?php esc_html_e( 'Only activate on', 'woo-notification' ) ?>
						<a href="https://goo.gl/PwXTzT"><?php echo esc_html__( 'Pro version ', 'woo-notification' ) ?></a>
					</p>
				</div>
				<div class="vi-ui bottom attached tab segment" data-tab="assign">
					<p>
						<?php esc_html_e( 'Only activate on', 'woo-notification' ) ?>
						<a href="https://goo.gl/PwXTzT"><?php echo esc_html__( 'Pro version ', 'woo-notification' ) ?></a>
					</p>
				</div>
				<div class="vi-ui bottom attached tab segment" data-tab="sound">
					<p>
						<?php esc_html_e( 'Only activate on', 'woo-notification' ) ?>
						<a href="https://goo.gl/PwXTzT"><?php echo esc_html__( 'Pro version ', 'woo-notification' ) ?></a>
					</p>
				</div>
				<div class="vi-ui bottom attached tab segment" data-tab="logs">
					<p>
						<?php esc_html_e( 'Only activate on', 'woo-notification' ) ?>
						<a href="https://goo.gl/PwXTzT"><?php echo esc_html__( 'Pro version ', 'woo-notification' ) ?></a>
					</p>
				</div>
				<div class="vi-ui bottom attached tab segment active" data-tab="general">
					<!-- Tab Content !-->
					<table class="optiontable form-table">
						<tbody>
						<tr valign="top">
							<th scope="row">
								<label for="<?php echo self::set_field( 'enable' ) ?>">
									<?php esc_html_e( 'Enable', 'woo-notification' ) ?>
								</label>
							</th>
							<td>
								<div class="vi-ui toggle checkbox">
									<input id="<?php echo self::set_field( 'enable' ) ?>"
										   type="checkbox" <?php checked( self::get_field( 'enable' ), 1 ) ?>
										   tabindex="0" class="" value="1"
										   name="<?php echo self::set_field( 'enable' ) ?>" />
									<label></label>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<!--Products-->
				<div class="vi-ui bottom attached tab segment" data-tab="products">
					<!-- Tab Content !-->
					<table class="optiontable form-table">
						<tbody>
						<tr valign="top" class="select_product">
							<th scope="row">
								<label><?php esc_html_e( 'Select Product', 'woo-notification' ) ?></label>
							</th>
							<td>
								<?php
								$products_ach = self::get_field( 'archive_products', array() ); ?>
								<select name="<?php echo self::set_field( 'archive_products', true ) ?>" class="product-search" placeholder="<?php esc_attr_e( 'Please select products', 'woo-notification' ) ?>" style="width: 100%">
									<?php if ( count( $products_ach ) ) {
										$args_p      = array(
											'post_type'   => array( 'product', 'product_variation' ),
											'post_status' => 'publish',
											'post__in'    => $products_ach
										);
										$the_query_p = new WP_Query( $args_p );
										if ( $the_query_p->have_posts() ) {
											$products_ach = $the_query_p->posts;
											foreach ( $products_ach as $product_ach ) {
												$data_ach = wc_get_product( $product_ach );
												if ( woocommerce_version_check() ) {
													if ( $data_ach->get_type() == 'variation' ) {
														$name_prd = $data_ach->get_name();
													} else {
														$name_prd = $data_ach->get_title();
													}
												} else {
													$prd_var_title = $data_ach->post->post_title;
													if ( $data_ach->get_type() == 'variation' ) {
														$prd_var_attr = $data_ach->get_variation_attributes();
														$attr_name1   = array_values( $prd_var_attr )[0];
														$name_prd     = $prd_var_title . ' - ' . $attr_name1;
													} else {
														$name_prd = $prd_var_title;
													}
												}
												if ( $data_ach ) { ?>
													<option selected="selected" value="<?php echo esc_attr( $data_ach->get_id() ); ?>"><?php echo esc_html( $name_prd ); ?></option>
												<?php }
											}
										}
										// Reset Post Data
										wp_reset_postdata();
									} ?>
								</select>
							</td>
						</tr>
						<tr valign="top" class="select_product">
							<th scope="row">
								<label><?php esc_html_e( 'Virtual First Name', 'woo-notification' ) ?></label>
							</th>
							<td>
                                <textarea
									name="<?php echo self::set_field( 'virtual_name' ) ?>"><?php echo self::get_field( 'virtual_name' ) ?></textarea>
								<p class="description"><?php esc_html_e( 'Virtual first name what will show on notification. Each first name on a line.', 'woo-notification' ) ?></p>
							</td>
						</tr>
						<tr valign="top" class="select_product">
							<th scope="row">
								<label><?php esc_html_e( 'Virtual Time', 'woo-notification' ) ?></label></th>
							<td>
								<div class="vi-ui form">
									<div class="inline fields">
										<input type="number" name="<?php echo self::set_field( 'virtual_time' ) ?>"
											   value="<?php echo self::get_field( 'virtual_time', '10' ) ?>" />
										<label><?php esc_html_e( 'hours', 'woo-notification' ) ?></label>
									</div>
								</div>
								<p class="description"><?php esc_html_e( 'Time will auto get random in this time threshold ago.', 'woo-notification' ) ?></p>
							</td>
						</tr>
						<tr valign="top" class="virtual_address ">
							<th scope="row">
								<label><?php esc_html_e( 'Virtual City', 'woo-notification' ) ?></label></th>
							<td>
                                <textarea
									name="<?php echo self::set_field( 'virtual_city' ) ?>"><?php echo self::get_field( 'virtual_city' ) ?></textarea>
								<p class="description"><?php esc_html_e( 'Virtual city name what will show on notification. Each city name on a line.', 'woo-notification' ) ?></p>
							</td>
						</tr>
						<tr valign="top" class="virtual_address ">
							<th scope="row">
								<label><?php esc_html_e( 'Virtual Country', 'woo-notification' ) ?></label></th>
							<td>
								<input type="text" name="<?php echo self::set_field( 'virtual_country' ) ?>"
									   value="<?php echo self::get_field( 'virtual_country' ) ?>" />
								<p class="description"><?php esc_html_e( 'Virtual country name what will show on notification.', 'woo-notification' ) ?></p>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<!-- Design !-->
				<div class="vi-ui bottom attached tab segment" data-tab="design">
					<!-- Tab Content !-->
					<table class="optiontable form-table">
						<tbody>
						<tr valign="top">
							<th scope="row">
								<label><?php esc_html_e( 'Message purchased', 'woo-notification' ) ?></label>
							</th>
							<td>
                                <textarea
									name="<?php echo self::set_field( 'message_purchased' ) ?>"><?php echo strip_tags( self::get_field( 'message_purchased', 'Someone in {city}, {country} purchased a {product_with_link} {time_ago}' ) ) ?></textarea>
								<ul class="description" style="list-style: none">
									<li>
										<span>{first_name}</span>
										- <?php esc_html_e( 'Customer first name', 'woo-notification' ) ?>
									</li>
									<li>
										<span>{city}</span> - <?php esc_html_e( 'Customer city', 'woo-notification' ) ?>
									</li>
									<li>
										<span>{country}</span>
										- <?php esc_html_e( 'Customer country', 'woo-notification' ) ?>
									</li>
									<li>
										<span>{product}</span>
										- <?php esc_html_e( 'Product title', 'woo-notification' ) ?>
									</li>
									<li>
										<span>{product_with_link}</span>
										- <?php esc_html_e( 'Product title with link', 'woo-notification' ) ?>
									</li>
									<li>
										<span>{time_ago}</span>
										- <?php esc_html_e( 'Time after purchase', 'woo-notification' ) ?>
									</li>
								</ul>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label><?php esc_html_e( 'Position', 'woo-notification' ) ?></label>
							</th>
							<td>
								<div class="vi-ui form">
									<div class="fields">
										<div class="four wide field">
											<img src="<?php echo VI_WNOTIFICATION_F_IMAGES . 'position_1.jpg' ?>"
												 class="vi-ui centered medium image middle aligned " />
											<div class="vi-ui toggle checkbox center aligned segment">
												<input id="<?php echo self::set_field( 'position' ) ?>"
													   type="radio" <?php checked( self::get_field( 'position', 0 ), 0 ) ?>
													   tabindex="0" class="" value="0"
													   name="<?php echo self::set_field( 'position' ) ?>" />
												<label><?php esc_attr_e( 'Bottom left', 'woo-notification' ) ?></label>
											</div>

										</div>
										<div class="four wide field">
											<img src="<?php echo VI_WNOTIFICATION_F_IMAGES . 'position_2.jpg' ?>"
												 class="vi-ui centered medium image middle aligned " />
											<div class="vi-ui toggle checkbox center aligned segment">
												<input id="<?php echo self::set_field( 'position' ) ?>"
													   type="radio" <?php checked( self::get_field( 'position' ), 1 ) ?>
													   tabindex="0" class="" value="1"
													   name="<?php echo self::set_field( 'position' ) ?>" />
												<label><?php esc_attr_e( 'Bottom right', 'woo-notification' ) ?></label>
											</div>
										</div>
										<div class="four wide field">
											<img src="<?php echo VI_WNOTIFICATION_F_IMAGES . 'position_4.jpg' ?>"
												 class="vi-ui centered medium image middle aligned " />
											<div class="vi-ui toggle checkbox center aligned segment">
												<input id="<?php echo self::set_field( 'position' ) ?>"
													   type="radio" <?php checked( self::get_field( 'position' ), 2 ) ?>
													   tabindex="0" class="" value="2"
													   name="<?php echo self::set_field( 'position' ) ?>" />
												<label><?php esc_attr_e( 'Top left', 'woo-notification' ) ?></label>
											</div>
										</div>
										<div class="four wide field">
											<img src="<?php echo VI_WNOTIFICATION_F_IMAGES . 'position_3.jpg' ?>"
												 class="vi-ui centered medium image middle aligned " />
											<div class="vi-ui toggle checkbox center aligned segment">
												<input id="<?php echo self::set_field( 'position' ) ?>"
													   type="radio" <?php checked( self::get_field( 'position' ), 3 ) ?>
													   tabindex="0" class="" value="3"
													   name="<?php echo self::set_field( 'position' ) ?>" />
												<label><?php esc_attr_e( 'Top right', 'woo-notification' ) ?></label>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
					<?php
					$class = array();
					switch ( self::get_field( 'position' ) ) {
						case 1:
							$class[] = 'bottom_right';
							break;
						case 2:
							$class[] = 'top_left';
							break;
						case 3:
							$class[] = 'top_right';
							break;
						default:
							$class[] = '';
					}
					$class[] = self::get_field( 'image_position' ) ? 'img-right' : '';
					?>
					<div style="display: block;" class="customized  <?php echo esc_attr( implode( ' ', $class ) ) ?>"
						 id="message-purchased">
						<img src="<?php echo esc_url( VI_WNOTIFICATION_F_IMAGES . 'demo-image.jpg' ) ?>">

						<p>Joe Doe in London, England purchased a
							<a href="#">Ninja Silhouette</a>
							<small>About 9 hours ago</small>
						</p>
						<span id="notify-close"></span>

					</div>
				</div>
				<p>
					<input type="submit" class="button button-primary"
						   value=" <?php esc_html_e( 'Save', 'wp-send-admin' ) ?> " />
				</p>
			</form>
			<?php do_action( 'villatheme_ads' ) ?>
		</div>
	<?php }
} ?>