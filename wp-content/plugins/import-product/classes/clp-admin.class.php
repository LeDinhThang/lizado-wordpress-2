<?php
	class CPL_Admin {
		public function __construct(){
			
			add_action( 'admin_menu', array(&$this,'register_page') );
			add_action( 'init', array(&$this,'register_assets') );
			
		}

		public function register_assets(){
			if( is_admin() && @$_GET['page'] == 'import-products'){
				wp_enqueue_style( 'cpl-boostrap-css-1', CLP_URI . 'assets/bootstrap.css' );
				
			}
			wp_enqueue_style( 'cpl-css-1', CLP_URI . 'assets/cpl.css' );
			wp_enqueue_script( 'cpl-js-form', CLP_URI . 'assets/jquery.form.js', array( 'jquery' ) );
			
			
			
		}
		
		function register_page(){
			add_menu_page( 'Import Products', 'Import Products', 'manage_options', 'import-products', array(&$this,'cpl_admin_page'), CLP_URI.'assets/star.png', 6 ); 
		}

		function cpl_admin_page(){
			
			$alerts = $this->check_write_able();
			$errors = array();
			if( isset( $_POST['upload_method'] )  && $_POST['upload_method'] == 'normal'){
				$CPL_Data = new CLP_Data();
				$CPL_Data->_params  = $_POST;
				$CPL_Data->_files 	= $_FILES;
				$result = $CPL_Data->upload_file(false);
				if( $result['status'] == 0 ){
					$errors[] = $result['msg'];
				}else{
					$options = $result;
					unset( $options['msg'] );
					unset( $options['status'] );
					$options['task'] 		= 'read_file';
					$options['task_force'] 	= 'read_file';
					$options['force'] 		= 1;
					$options['parrent_cat'] = $_POST['parrent_cat'];
					update_option( 'clp_options' ,$options);
					?>
						<script type="text/javascript">
							var url = "<?php echo home_url().'/wp-admin/admin.php?page=import-products&force=1'; ?>";
							window.location.href = url;
						</script>
					<?php

				}
			}
			?>
			<div style="clear:both;margin-top:10px;"></div>
			<?php
				if( count($alerts) > 0 || count($errors) ){
					?>
						<div class="container">       
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Please fix folowing errors</h3>
										</div>
										<div class="panel-body">
											<ul>
												<?php
													if( count($alerts) > 0 ){
														foreach ($alerts as $alert) {
															echo '<li style="color:red;">'.$alert.'</li>';
														}	
													}
													if( count($errors) > 0 ){
														foreach ($errors as $error) {
															echo '<li style="color:red;">'.$error.'</li>';
														}	
													}
													
												?>
											</ul>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php
				}
			?>
			<div class="container">       
				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Import Products</h3>
							</div>
							<div class="panel-body">
							  
								<?php 
									if( count($alerts) == 0 ){
										include_once CLP_ABSPATH.'views/dashboard-form.php';										
									}
								;?>
							   
							  
							</div>
						</div>
					</div>
					<div id="right">                        
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Processing</h3>
						</div>
						<div class="panel-body txt_process">
						   
						</div>
					</div>
					</div>
				</div>

			</div>
			<?php
		}

		function check_write_able(){
			$msg = array();
			if( !is_writeable( CLP_ABSPATH.'pro_data' ) ){
				$msg[] =  'Folder <strong>'.CLP_ABSPATH.'pro_data </strong> can not write able !' ;
			}
			if( !is_writeable( CLP_ABSPATH.'tmp_data' ) ){
				$msg[] =  'Folder <strong>'.CLP_ABSPATH.'tmp_data </strong> can not write able !' ;
			}
			if( !is_writeable( CLP_ABSPATH.'tmp_images' ) ){
				$msg[] =  'Folder <strong>'.CLP_ABSPATH.'tmp_images </strong> can not write able !' ;
			}

			if ( !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			  	$msg[] =  'Plugin <strong>Woocommerce</strong> is not actived or not installed. Please download it at <a target="_blank" href="https://wordpress.org/plugins/woocommerce/">Here</a> !' ;
			}

			return $msg;
		}
	}