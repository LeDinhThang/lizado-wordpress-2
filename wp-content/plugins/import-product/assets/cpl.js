var cpl_ajax_url = ajaxurl;
jQuery( document ).ready( function($){
	var txt_process = $('.txt_process');
    $('#start').on('click',function(e){
        e.preventDefault();
        var dta = $( '#import_form' ).serializeArray();

        // $('#form_filedset').attr('disabled','disabled');
        // $('#start').attr('disabled','disabled');
        // $('#reset').attr('disabled','disabled');
        
        $('.txt_process').text( 'Spara alternativ...' );
       
        $.ajax({
            type: "POST",
            url: cpl_ajax_url,
            //action: 'cpl_admin_start',
            data: dta,
            dataType: "json"
        })
        .done(function( respon ) {
            $('.txt_process').text( 'Saving Options Done' );
            if( respon.status == 1 ){
               show_links(respon.step);
            }
        });
    });


    //==========================functions ==========================
     function show_links( step ){
		   var dta = {
		        'action' : 'clp_admin_show_links',
		   };
		    $('.txt_process').text( 'Showing Links...' );
		    $.ajax({
		        type: "POST",
		        url: cpl_ajax_url,
		        data: dta,
		        dataType: "json",
				cache: false,
		    })
		    .done(function( respon ) {
		        $('.txt_process').text( 'Showing Links Done !' );
		        if( respon.status == 1 ){
		           $('#div_result').html(respon.html);
		           var params = $('input[ name="clp[nelly][params]" ]').val();
		           var task = $('input[ name="clp[nelly][task]" ]').val();
		           var stepname = $('input[ name="clp[nelly][stepname]" ]').val();
		           var sub = $('input[ name="clp[nelly][sub]" ]').val();
		           var page = $('input[ name="clp[nelly][page]" ]').val();
				   if( typeof sub == 'undefined' ){
					  sub = -1;
				   }
				   if( typeof page == 'undefined' ){
					  page = 1;
				   }
		           get_cat(step,task,params,sub,page,stepname);
		        }
		    });
	} 
	function get_cat(step,task,params,sub,page,stepname){
		   var dta = {
		        'action' : 'clp_admin_get_cat',
		        'step'  : step,
		        'id'  	: 1,
		        'task'  : task,
		        'params': params,
		        'sub' 	: sub,
		        'page' 	: page,
		        'stepname' 	: stepname,
		   };
		    $('.txt_process').text( 'F� kategorier ...' );
		    $.ajax({
		        type: "POST",
		        url: cpl_ajax_url,
		        data: dta,
		        dataType: "json",
				cache: false,
		    })
		    .done(function( respon ) {
		        $('.txt_process').text( 'Showing Links Done !' );
		        if( respon.status == 1 ){
		           if( typeof respon.sub == 'undefined' ){
		           		respon.sub = -1;
		           }	
		           if( typeof respon.page == 'undefined' ){
		           		respon.page = 0;
		           }
		           if( typeof respon.params == 'undefined' ){
		           		respon.params = 0;
		           }
				    if( typeof respon.stepname == 'undefined' ){
		           		respon.stepname = 'step1';
		           }

		          
			           	get_cat(respon.step,respon.task,respon.params,respon.sub,respon.page,respon.stepname);
			           var str = 'Task: ' + respon.task +  '; Key: '+ respon.params + '; Sub: '+respon.sub + '; Page: ' + respon.page;
			            $('.txt_process').text( str );
			            var id = 1;
			            $('#nation_'+id).text('Working...');
		          
		           
		        }
		    });
	} 

	function process_data(){
		var dta = {
		        'action' : 'clp_admin_process_data',
		   };
		$.ajax({
		        type: "POST",
		        url: cpl_ajax_url,
		        data: dta,
		        dataType: "json"
		    })
		    .done(function( respon ) {
		        $('.txt_process').text( 'Showing Links Done !' );
		        if( respon.status == 1 ){
		            $('#nation_1').text('Process data...');
		        }
		    });
	}



} );

